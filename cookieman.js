import { compareAsc } from "date-fns";

/**
 * This tool class aims to provide basic cookie features like
 * adding cookies, getting the full list
 * 
 * Note : every cookie is create with a 2 years default TTL
 */
export default class CookieMan {

    /**
     * user consent boolean
     * @param {boolean} consent 
     */
    constructor(consent=false){
        this.consent=consent;
    }

    /**
     * Get the user content boolean variable
     */
    get consent(){
        if (CookieMan.hasCookie("consent"))
            this.consent=true;
        return this._consent;
    }

    /**
     * Set the content with a boolean
     */
    set consent(consent){
        this._consent=consent;
        this.setCookie("consent", (new Date).toUTCString());
    }

    /**
     * Get a cookie from its name
     * @param {string} name 
     * @returns 
     */
    static getCookie(name) {
        // match ; name=<value>.
        const matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    /**
     * Return an array of cookie object with {name,value}
     * @returns an array of  of cookies object
     */
    static getCookiesArray() {
        const result = document.cookie?.split(";");
        return result.map(element => {
            const [name, value] = element.split("=");
            return { name: name.trim(), value: value.trim() };
        });
    }


    /**
     * Set a cookie and its options.
     * Note : By default 'max-age' is set for 2 years, expires is less priority than 'max-age'.
     * If you are to update this, please use 'max-age'
     * 
     * @param {string} name Name of the cookie
     * @param {*} value Value of the cookie
     * @param {object} Options options of the cookie
     * @example setCookie('user', 'John', {secure: true, 'max-age': 3600});
     */
    setCookie(name, value, options = {}) {
        if (!this._consent){
            return;
        }
        const maxAge= 60*60*24*365*2; // 2 years
        options = {
            path: '/',
            'max-age': maxAge,
            // add other defaults here if necessary
            ...options
        };

        if (options.expires instanceof Date) {
            options.expires = options.expires.toUTCString();
        }

        let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

        for (const optionKey in options) {
            updatedCookie += "; " + optionKey;
            let optionValue = options[optionKey];
            if (optionValue !== true) {
                updatedCookie += "=" + optionValue;
            }
        }
        document.cookie = updatedCookie;
    };

    /**
     * Delete a cookie
     * @param {string} name Name of the cookie
     */
    static deleteCookie(name) {
        CookieMan.setCookie(name, "", {
            'max-age': -1
        })
    }

    /**
     * Check if a cookie exists
     * @param {string} name 
     * @returns true if the cookie exists
     */
    static hasCookie(name) {
        return CookieMan.getCookie(name) ? true : false;
    }

}